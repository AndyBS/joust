
A game in the spirit of "Johan Sebastian Joust" for the flow3r.

Created by Andy Barrett-Sprot.

Joust is a multiplayer physical game. The core of the game is to be the last one standing you're knocked out when your badge moves too quickly. The aim is to 'attack' your players by nudging their badges, whilst defending yours.

Instructions:

1. Select 'Start' from the menu
1. The game starts when all flow3r badges are green.
1. Hold your badge firmly in your hand, using the lanyard as a strap.
1. The light goes red if you move your badge too quickly. If you move it too quickly, you're knocked out and should wait for the next round.
1. Keep playing until only 1 player has a green light, then claim victory.
