# flow3r Joust
# Based on the concept from https://github.com/adangert/JoustMania

import st3m.run
import leds
import bl00mbox
from st3m.application import Application, ApplicationContext
from st3m.ui.view import BaseView, ViewManager
from st3m.ui.colours import PUSH_RED, GO_GREEN, BLACK
from st3m.input import InputState

blm = bl00mbox.Channel("Joust")

COLOR_GREEN=(0,1,0)
COLOR_RED=(1,0,0)
COLOR_YELLOW=(1,0.6,0)

class Menu(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self.selected = 0
        self.items = ["Instructions", "Start"]

    def on_enter(self, vm: Optional[ViewManager]) -> None:
        leds.set_all_rgb(*COLOR_YELLOW)
        leds.update()
        super().on_enter(vm)

    def draw(self, ctx: Context) -> None:
        # Paint the background yellow
        ctx.rgb(255, 200, 0).rectangle(-120, -120, 240, 240).fill()
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.font_size = 50
        ctx.font = ctx.get_font_name(5)
        ctx.rgb(0,0,0)
        ctx.move_to(0, -70)
        ctx.text("Joust")
        self.render_menu(ctx)

        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.font_size = 15
        ctx.font = ctx.get_font_name(0)
        ctx.rgb(0,0,0)
        ctx.move_to(0, -45)
        ctx.text("v1.0.0")

    def render_menu(self, ctx: Context) -> None:
        separation = 15
        offset = 0 - (self.selected * (separation + 15))
        for i, text in enumerate(self.items):
            ctx.text_align = ctx.CENTER
            ctx.text_baseline = ctx.TOP
            ctx.font_size = 25
            ctx.move_to(0, offset + separation * i)
            if i == self.selected:
                font_id = 1 # Bold
                text = "> " + text + " <"
            else:
                font_id = 0 # Regular
            ctx.font = ctx.get_font_name(font_id)
            ctx.text(text)
            offset = offset + separation

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)

        if self.input.buttons.app.middle.pressed:
            if self.selected == 0:
                # Instructions menu
                self.vm.push(Info())
            elif self.selected == 1:
                # Start menu
                print("Starting countdown")
                self.vm.push(Countdown())

        if self.input.buttons.app.left.pressed:
            self.selected = self.selected - 1

        if self.input.buttons.app.right.pressed:
            self.selected = self.selected + 1
        # Cap to menu items
        self.selected = max(min(self.selected, 1), 0)

    def on_exit(self):
        leds.set_all_rgb(*BLACK)
        leds.update()

class Countdown(BaseView):
    def __init__(self) -> None:
        super().__init__()
        self.countdown = 3000
        self.leaving = False
        self.prev_leds = False

    def on_enter(self, vm: Optional[ViewManager]) -> None:
        super().on_enter(vm)

    def draw(self, ctx: Context) -> None:
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.font_size = 100
        ctx.font = ctx.get_font_name(5)

        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
        ctx.rgb(0,255,0)

        ctx.move_to(0, 0)

        # Paint the background Red
        if self.countdown > 2000:
            text = "3"
        elif self.countdown > 1000:
            text = "2"
        else:
            text = "1"
        ctx.text(text)

    def think(self, ins: InputState, delta_ms: int) -> None:
        self.input.think(ins, delta_ms)
        self.countdown = self.countdown - delta_ms
        # Make the LEDs blink
        blink = ((self.countdown / 250) % 2) > 1

        if self.prev_leds != blink and not self.leaving:
            self.prev_leds = blink
            if blink:
                leds.set_all_rgb(*COLOR_YELLOW)
            else:
                leds.set_all_rgb(*COLOR_GREEN)
            leds.update()
        if self.countdown < 0:
            print("Countdown finished, starting game")
            self.leaving = True
            self.vm.replace(Running())
    def on_exit(self):
        leds.set_all_rgb(*BLACK)
        leds.update()


class Running(BaseView):
    threshold = 13

    def __init__(self) -> None:
        super().__init__()

    def on_enter(self, vm: Optional[ViewManager]) -> None:
        super().on_enter(vm)
        leds.set_all_rgb(0,1,0)
        leds.update()

    def draw(self, ctx: Context) -> None:
        # Paint the background Green
        ctx.rgb(0, 255, 0).rectangle(-120, -120, 240, 240).fill()
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.font_size = 75
        ctx.font = ctx.get_font_name(5)
        ctx.rgb(0,0,0)
        ctx.move_to(0, 0)
        ctx.text("IN")

    def think(self, ins: InputState, delta_ms: int) -> None:
        self.input.think(ins, delta_ms)
        if self.is_jerked(ins.imu.acc):
            print("Too much movement, entering lost state")
            self.vm.replace(Lost())

    def is_jerked(self, acc):
        acceleration = (acc[0]**2 + acc[1]**2 + acc[2]**2)**0.5
        return acceleration > Running.threshold

    def on_exit(self):
        leds.set_all_rgb(*BLACK)
        leds.update()

class Lost(BaseView):
    def __init__(self) -> None:
        super().__init__()
        self.synth = blm.new(bl00mbox.patches.tinysynth)
        self.synth.signals.output = blm.mixer
        self.synth.signals.decay = 50
        self.synth.signals.pitch.tone = 9
        self.timer = 0
        self.leaving = False
        self.current_blink = True

    def on_enter(self, vm: Optional[ViewManager]) -> None:
        leds.set_all_rgb(1,0,0)
        leds.update()
        super().on_enter(vm)

    def draw(self, ctx: Context) -> None:
        # Paint the background Red
        ctx.rgb(255, 0, 0).rectangle(-120, -120, 240, 240).fill()
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.font_size = 75
        ctx.font = ctx.get_font_name(5)
        ctx.rgb(0,0,0)
        ctx.move_to(0, 0)
        ctx.text("OUT")

    def think(self, ins: InputState, delta_ms: int) -> None:
        self.input.think(ins, delta_ms)
        self.timer = self.timer + delta_ms

        blink = ((self.timer / 250) % 2) < 1
        if (self.current_blink != blink) and not self.leaving:
            self.current_blink = blink
            if blink:
                leds.set_all_rgb(*COLOR_RED)
                if self.timer < 4000:
                    self.synth.signals.trigger.start()
            else:
                self.synth.signals.trigger.stop()
                leds.set_all_rgb(*BLACK)
            leds.update()

        if self.input.buttons.app.middle.pressed:
            print("Re-entering menu")
            self.leaving = True
            self.synth.signals.trigger.stop()
            self.vm.pop()

    def on_exit(self):
        self.synth.signals.trigger.stop()
        leds.set_all_rgb(*BLACK)
        leds.update()


class Info(BaseView):

    def draw(self, ctx: Context) -> None:
        # Paint the background Yellow
        ctx.rgb(255, 200, 0).rectangle(-120, -120, 240, 240).fill()
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.TOP
        ctx.font_size = 40
        ctx.font = ctx.get_font_name(5)
        ctx.rgb(0,0,0)
        ctx.move_to(0, -100)
        ctx.text("How To Play")

        instructions = \
        "Joust is a multiplayer game\nplayed with multiple badges:\n" +\
        "1. Hold the badge steady in\n    your hand.\n" +\
        "2. If it moves too fast, you\n    are out.\n" +\
        "3. Gently knock your opponents\n    arm to knock them out.\n" +\
        "4. Last surviving wins."
        ctx.text_align = ctx.LEFT
        ctx.text_baseline = ctx.MIDDLE
        ctx.font_size = 15
        ctx.font = ctx.get_font_name(0)
        ctx.rgb(0,0,0)
        ctx.move_to(-90, -55)
        ctx.text(instructions)

        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.BOTTOM
        ctx.font_size = 30
        ctx.font = ctx.get_font_name(0)
        ctx.rgb(0,0,0)
        ctx.move_to(0, 100)
        ctx.text("< Back")


    def think(self, ins: InputState, delta_ms: int) -> None:
        self.input.think(ins, delta_ms)

        if self.input.buttons.app.middle.pressed or self.input.buttons.app.right.pressed or self.input.buttons.app.left.pressed:
            print("Re-entering menu")
            self.vm.pop()

    def on_exit(self):
        leds.set_all_rgb(*BLACK)
        leds.update()

if __name__ == '__main__':
    st3m.run.run_view(Menu(ApplicationContext()))
